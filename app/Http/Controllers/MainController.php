<?php

namespace App\Http\Controllers;

use App\Http\Fibonacci;
use Exception;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function fibonacci(Request $request)
    {
        $data = $request->all();
        $this->validate($request,
            ['sequenceStartingNumber' =>'required|numeric']
        );
        $data['sequenceLength'] = isset($data['sequenceLength']) ? $data['sequenceLength'] : 10;
        try {
            $fibonacciSequence = Fibonacci::getFibonacciSequence($data['sequenceLength'],$data['sequenceStartingNumber']);

            $textToWrite = 'The fibonacci sequence with length '.$data['sequenceLength'].' starting from '.$data['sequenceStartingNumber'].
            ' is: ';
            $textToWrite .= implode(" ", $fibonacciSequence)."\n";
            file_put_contents(public_path()."/results.txt",$textToWrite,LOCK_EX);

            return redirect()->back()->with('message','Success! Output has been saved to a file.')->with('output', implode(" ", $fibonacciSequence));

        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

}
