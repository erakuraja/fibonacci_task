<?php
/**
 * Created by PhpStorm.
 * User: kuraj
 * Date: 20/01/2019
 * Time: 23.58
 */

namespace App\Http;


use Exception;

class Fibonacci
{
    /**
     * @param int $sequenceLength
     * @param int $number1
     * @return array
     * @throws Exception
     */
    public static function getFibonacciSequence($sequenceLength = 10, $number1)
    {
        Fibonacci::validateNumbers($sequenceLength, $number1);

        $isFibonacci = Fibonacci::isFibonacci($number1) == 1;

        if (!$isFibonacci) {
            $number1 = Fibonacci::getNextFibonacciNumber($number1);
        }
        $number2 = Fibonacci::getNextFibonacciNumber($number1);

        $count = 0;
        $numbers = [];
        while ($count < $sequenceLength){
            $numbers[] = $number1;
            $temp = $number2 + $number1;
            $number1 = $number2;
            $number2 = $temp;
            $count = $count + 1;
        }
        return $numbers;
    }

    /**
     * @param $number
     * @return mixed
     */
    public static function getNextFibonacciNumber($number) {

        $fib = array($number+1);       // array to num + 1
        $fib[0] = 0; $fib[1] = 1;
        $i;

        for ($i=2;$i<=$number+1;$i++) {
            $fib[$i] = $fib[$i-1]+$fib[$i-2];
            if ($fib[$i] > $number) { // check if key > num
                return $fib[$i];
            }
        }
        if ($fib[$i-1] < $number) {   // check if key < num
            return $fib[$i-1] + $number;
        }
        if ($fib[$i] = $number-1) {   // check if key = num
            return $fib[$i-1] + $fib[$i-2];
        }
        if ($fib[$i-1] = 1) {    // check if num = 1
            return $number + $number;
        }
    }

    /**
     * @param $number
     * @return bool
     */
    public static function isFibonacci($number) {
        $test_1 = sqrt(5 * pow($number, 2) + 4);
        $test_2 = sqrt(5 * pow($number, 2) - 4);

        return ctype_digit((string)$test_1) || ctype_digit((string)$test_2);
    }

    /**
     * @param $sequenceLength
     * @param int $number1
     * @throws Exception
     */
    public static function validateNumbers($sequenceLength, int $number1)
    {
        if (!is_numeric($sequenceLength) || !is_numeric($number1)) {
            throw new Exception("Sequence Length or Starting Point are not numbers");
        }

        if ($sequenceLength < 0 || $number1 < 0) {
            throw new Exception ("Sequence Length or Starting Point should not be negative.");
        }
    }
}