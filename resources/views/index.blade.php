<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fibonacci task - Software Maintenance: Assignment 1</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" type="text/javascript">

</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content" style="margin-top: 70px;">

        <div class="container">
            <form method="post" action="/retrieveFibonacci">
                {!! csrf_field() !!}

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                @if(session()->has('output'))
                    <div class="alert alert-dark">
                        The following sequence added: {{ session()->get('output') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="exampleInputEmail1">Length of the Fibonacci sequence:</label>
                    <input type="number" class="form-control" name="sequenceLength" id="sequenceLength" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Starting number of the Fibonacci sequence:</label>
                    <input type="number" class="form-control" name="sequenceStartingNumber" id="sequenceStartingNumber">
                </div>

                <button style="width: 100%;" type="submit" class="btn btn-success">Generate Sequence</button>
            </form>
        </div>

    </div>
</div>
</body>
</html>