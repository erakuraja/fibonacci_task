<?php

use App\Http\Fibonacci;

/**
 * Created by PhpStorm.
 * User: kuraj
 * Date: 20/01/2019
 * Time: 23.57
 */

class FibonacciTests extends \PHPUnit\Framework\TestCase
{
    private $fibonacci;

    protected function setUp()
    {
        $this->fibonacci = new Fibonacci();
    }

    protected function tearDown()
    {
        $this->fibonacci = NULL;
    }

    public function testOutputResults()
    {
        $result = \App\Http\Fibonacci::getFibonacciSequence(4, 5);


        $this->assertEquals([5, 8, 13, 21], $result);
    }

    public function testInputAttributes()
    {
        $this->expectException(ArgumentCountError::class);
        $result = Fibonacci::getFibonacciSequence(4);
    }

    public function testInvalidNegativeAttributes()
    {
        $this->expectExceptionMessage("Sequence Length or Starting Point should not be negative.");
        $result = Fibonacci::getFibonacciSequence(-4,-5);
    }
}